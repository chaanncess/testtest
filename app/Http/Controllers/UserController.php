<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function index(): JsonResponse
    {
        $users = User::all();

        return response()->json($users);
    }

    public function delete(User $user): JsonResponse
    {
        $user->delete();

        return response()->json(['message' => 'успешно удалён']);
    }

    public function changeActivity(Request $request, User $user): JsonResponse
    {
        $user->update(['active' => $request->input('active')]);

        return response()->json(['message' => 'активность пользователя успено изменена']);
    }

    /**
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'login' => 'required',
            'full_name' => 'required',
            'phone' => 'required',
        ]);

        // Создание нового пользователя
        $user = new User();
        $user->login = $request->input('login');
        $user->full_name = $request->input('full_name');
        $user->phone = $request->input('phone');
        $user->active = true;
        $user->save();

        return response()->json($user, 201);

    }
}
